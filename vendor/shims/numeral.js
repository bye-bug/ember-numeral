(function() {
  function vendorModule() {
    'use strict';

    return {
      'default': self['numeral'],
      __esModule: true,
    };
  }

  define('numeral', [], vendorModule);
})();
