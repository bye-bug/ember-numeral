'use strict';

module.exports = {
  name: require('./package').name,
  included(app) {
    app.import('node_modules/numeral/numeral.js');
    app.import('vendor/shims/numeral.js');
  }
};
