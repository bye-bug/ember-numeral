import { helper } from '@ember/component/helper';
import numeral from 'numeral';

export function formatNumeral(number, format) {
  return numeral(number).format(format.format);
}

export default helper(formatNumeral);
